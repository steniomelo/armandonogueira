<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'armandonogueira');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'root');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/ -NnMJI-KI&z5` 09Pv2y|_Z^OA]ztH!>Std}Fql*/:WZm~BrJphs j#A4%+b}w');
define('SECURE_AUTH_KEY',  '|-Gnk9+uMT>?gI7B6K`m3,[*HFDQrcXqI9osxVu_RfZNutsiH.e9-%6MPEFyOL#|');
define('LOGGED_IN_KEY',    'MD/ ?mw2f[`>5`qwgM@~ocbVX/Z=q(jbYdM5@0D?3-cY$$=fmNFE3mw7p}MQQ+m<');
define('NONCE_KEY',        '0nY=Fq<vjO7WjiE3|PyDpx.}<;<mCk +G{5Ak(|j|{KlY1(6aW70k])WNMJ&{WzW');
define('AUTH_SALT',        'ym]#s|22]d`=hYjjd9{xZgyPr.<)a|6Fo]@/D0FV bBGUD~oS~N5=G)txJQ>|_qe');
define('SECURE_AUTH_SALT', 'kq]%7sa:Nw27c,gR ;=W42807y}_d<?E ?~Y<J)_lKqe#MqBL]q;Ix(d!R]b:i>z');
define('LOGGED_IN_SALT',   '_90d1ew$%Ki>okm{6>yatA1H_-ff9hgS(JG{ `m<SE`OG*w2{nfqZF8<{Q;/q7r4');
define('NONCE_SALT',       ' XM%66$|ae]n%wIO`)DxDSJ!=G(W<.}2t$wLj3H.CIHs:Mxm$!oh;/w4O7.l.6ez');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'an_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', true);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
