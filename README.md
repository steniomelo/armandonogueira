Para rodar o projeto:

1. Apague o arquivo wp-config e configure um novo banco de dados local vazio e acesse a url para iniciar uma nova instalação do Wordpress.
2. Ative o plugin "All in one Wp-migration'
3. Vá até o plugin e clique em importar. Importe o arquivo 'backup.wpress'
4. Use login: voltsstudio / vs321#@! para acessar o wp-admin


Para rodar a interface front-end:

1. Navegue até a parta wp-content/themes/armandonogueira
2. Use: "npm install"
3. Use: "gulp"

obs: "Configure o gulp.js de acordo com sua url local"

