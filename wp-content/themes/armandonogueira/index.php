<?php
/**
 * Template Name: Home

 */

get_header(); 

$args = array(
	'post_type'      => 'destaques',
	'post_status'    => 'publish',
	'posts_per_page' => 1,
	'meta_query' => array(
		array(
			'key'     => 'slider',
			'value'   => true,
			'compare' => 'LIKE',
		),
	),
);
$slider = new WP_Query( $args );

if ( $slider->have_posts() ) :  
	while ( $slider->have_posts() ) : $slider->the_post(); 

	$imoveis = get_field('imoveis');

?>

	<div id="destaques-slider">
		<div id="slides" data-destaque-slider>

		<?php foreach($imoveis as $imovel) { $imovel = json_decode($imovel);?>
			<div class="slide">
				<div class="slide-text">
					<h1><?php echo $imovel->nomeBairro;?></h1>
					<h3><?php echo $imovel->nomeImovel;?></h3>
				</div>

				<div class="slide-footer">
					<div class="container">
						<div class="slide-info">
							<?php if($imovel->areautil) : ?>
							<div class="info info-tamanho">
								<img src="<?php echo ASSETS;?>/img/icons/tamanho.svg">
								<span><?php echo formatarArea($imovel); ?></span>
							</div>
							<?php endif; ?>
							<div class="info info-quartos">
								<img src="<?php echo ASSETS;?>/img/icons/quarto.svg">
								<span><?php echo formatarQuartos($imovel); ?></span>
							</div>
							<div class="info info-banheiros">
								<img src="<?php echo ASSETS;?>/img/icons/banheiro.svg">
								<span><?php echo formatarBanheiros($imovel); ?></span>
							</div>
							<div class="info info-vagas">
								<img src="<?php echo ASSETS;?>/img/icons/vaga.svg">
								<span><?php echo formatarVagas($imovel); ?></span>
							</div>
							<div class="info info-valor">
								<img src="<?php echo ASSETS;?>/img/icons/valor.svg">
								<span>R$ <?php echo number_format($imovel->preco, 2, ',', '.'); ?></span>
							</div>
							<div class="info more-info">
								<a href="<?php echo ROTA_IMOVELDETALHE.$imovel->codigoImovel;?>" class="more-info-btn">
									<span class="d-none d-md-block"><?php echo $GLOBALS['campos']['home']['botao_do_slider'];?></span>
									<span class="d-block d-md-none"><?php echo $GLOBALS['campos']['home']['botao_do_slider_-_mobile'];?></span>
									<i class="arrow-hover"></i>								
								</a>
							</div>
						</div>
					</div>
				</div>

				<div class="slide-image">
					<?php if($imovel->urlFotoDestaque) : ?>
						<img src="<?php echo $imovel->urlFotoDestaque;?>">
					<?php else : ?>
						<img src="<?php echo $imovel->fotoImovelList[0]->url;?>">
					<?php endif; ?>
				</div>
			</div>
			<?php } ?>

		</div>
		
		<div id="slider-nav">
			<div class="container">
				<div class="nav-list" data-destaque-slider-nav>
				<?php $i = 0; foreach($imoveis as $imovel) { $imovel = json_decode($imovel); $i++; ($i == 1) ? $active = 'active' : $active = '';  ?>
					<div class="list-item <?php echo $active; ?>">
						<strong><?php echo $imovel->nomeBairro;?></strong>
						<small><?php echo $imovel->nomeImovel;?></small>
					</div>
				<?php } ?>
					<div class="list-item">
					</div>
				</div>
			</div>
		</div>
	</div>

<?php endwhile; 
wp_reset_postdata();
endif; ?>


<?php 
$args = array(
	'post_type'      => 'destaques',
	'post_status'    => 'publish',
	'posts_per_page' => -1,
	'meta_query' => array(
		array(
			'key'     => 'slider',
			'value'   => true,
			'compare' => 'NOT LIKE',
		),
	),
);
$destaques = new WP_Query( $args );

?>

<?php if ( $destaques->have_posts() ) :  
	while ( $destaques->have_posts() ) : $destaques->the_post(); 

	$imoveis = get_field('imoveis');
?>

	<section id="destaques" class="section">
		<header class="section-header">
			<h2 class="section-title"><?php the_title(); ?></h2>
		</header>

		<div class="section-content">
			<div class="list-horizontal imoveis " >
				<div class="list-items list-imoveis " data-list-horizontal>
					<?php foreach($imoveis as $imovel) {  
					
					$imovel = json_decode($imovel);
					
					?>
					<div class="list-item imovel">
						<div class="imovel-container">
							<div class="imovel-content">
								<div class="imovel-img">
									<?php if($imovel->urlFotoDestaque) { ?>
										<img src="<?php echo $imovel->urlFotoDestaque;?>">
									<?php } else { ?>
										<img src="<?php echo $imovel->fotoImovelList[0]->url;?>">
									<?php } ?>
								</div>

								<div class="imovel-details">
									<header class="imovel-title">
										<h2 class="imovel-bairro"><?php echo $imovel->nomeBairro; ?></h2>
										<h3 class="imovel-nome"><?php echo $imovel->nomeImovel; ?></h3>
									</header>
									<div class="imovel-info">
										<?php if($imovel->areautil) :?>
										<div class="info info-tamanho">
											<img src="<?php echo ASSETS;?>/img/icons/tamanho.svg">
											<small><?php echo formatarArea($imovel); ?></small>
										</div>
										<?php endif; ?>
										<div class="info info-quartos">
											<img src="<?php echo ASSETS;?>/img/icons/quarto.svg">
											<small><?php echo formatarQuartos($imovel); ?></small>
										</div>
										<div class="info info-banheiros">
											<img src="<?php echo ASSETS;?>/img/icons/banheiro.svg">
											<small><?php echo formatarBanheiros($imovel); ?></small>
										</div>
									</div>

									<div class="imovel-description">
										<?php 
										if (strlen($imovel->descricao) > 200) {
											echo substr($imovel->descricao, 0, 200).'...'; 
										} else {
											echo $imovel->descricao;
										}
										?>
									</div>

									<div class="imovel-price">
										<small><?php echo $GLOBALS['campos']['config']['geral']['a_partir_de']; ?></small>
										<strong class="price">
											<small class="price-cifrao">R$</small>
											<span class="price-valor"><?php echo number_format($imovel->preco, 2, ',', '.'); ?></span>
										</strong>
									</div>
								</div>
							</div>


						</div>
						<div class="imovel-footer">
							<a href="<?php echo ROTA_IMOVELDETALHE.$imovel->codigoImovel;?>" class="btn btn-block btn-primary"><?php echo $GLOBALS['campos']['home']['botao_dos_imoveis'];?></a>
						</div>
					</div>
					<?php } ?>
					<div class="space"></div>
				</div>
			</div>
		</div>
	</section>

<?php endwhile; 
wp_reset_postdata();
endif; ?>


<?php 
	$args = array(
		'post_type'      => 'post',
		'post_status'    => 'publish',
		'posts_per_page' => 5,
	);
	$noticias = new WP_Query( $args );

	$args = array(
		'post_type'      => 'post',
		'post_status'    => 'publish',
		'posts_per_page' => 6,
		'offset'		=> 5
	);
	$noticias_semimagem = new WP_Query( $args );
?>

<?php if ( $noticias->have_posts() ) :  ?>

<section id="noticias" class="section list-noticias d-none d-md-block">

	<div class="section-content">
		<div class="list-horizontal" >
			<header class="section-header">
				<h2 class="section-title">
					<?php echo $GLOBALS['campos']['config']['geral']['noticias']; ?>
				</h2>

				<nav class="section-nav">
					<span class="arrow arrow-prev">
						<img src="<?php echo ASSETS;?>/img/icons/arrow-right.svg">
					</span>
					<span class="arrow arrow-next">
						<img src="<?php echo ASSETS;?>/img/icons/arrow-right.svg">
					</span>
				</nav>
			</header>
			<div class="lists" data-list-horizontal-nobar>
				<div class="list-items">
					<?php 
						while ( $noticias->have_posts() ) : $noticias->the_post(); 
					?>
						<?php 
							get_template_part( 'partials/noticia', 'card' );
						?>
					<?php endwhile; 
					wp_reset_postdata(); ?>
					<div class="space"></div>

					
				</div>
				<div class="list-items sem-imagem">
					<?php 
						while ( $noticias_semimagem->have_posts() ) : $noticias_semimagem->the_post(); 
					?>
						<?php 
							get_template_part( 'partials/noticia', 'card' );
						?>
					<?php endwhile; 
					wp_reset_postdata(); ?>
					<div class="space"></div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>

<?php get_footer(); ?>