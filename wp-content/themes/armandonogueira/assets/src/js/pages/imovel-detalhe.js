( function( $ ) {

    $('a.vizinhanca').on('shown.bs.tab', function (e) {
        streetView();
        e.target // newly activated tab
        e.relatedTarget // previous active tab
    })

    function galeriaFoto() {
        $('.galeria-foto.slick-active').hover(function () {
        // $(this).css('width', '36%');
            $(this).addClass('current');
            //$(this).siblings().css('width', '16%');  
            $(this).siblings('.slick-active').addClass('notCurrent');
        }, function () {
            //$('.imgWrap').css('width', '20%');  
            $('.galeria-foto').siblings().removeClass('notCurrent');
            $('.galeria-foto').removeClass('current');
        });
    }

    function slickGaleria() {
        $galeriaCount = $("[data-destaque-galeria]").find('.galeria-foto').length;

        if($galeriaCount > 4) {
            $("[data-destaque-galeria]").on('init', function() {
                galeriaFoto();
            });
            $("[data-destaque-galeria]").on('afterChange', function() {
                galeriaFoto();
            });
            $("[data-destaque-galeria]").slick({
                infinite: false,
                autoplay: false,
                slidesToShow: 5,
                pauseOnHover: false,
                pauseOnFocus: false,
                arrows: true,
                lazyLoad: 'ondemand',
                prevArrow: $('.arrow-prev'),
                nextArrow: $('.arrow-next'),
                responsive: [
                    {
                      breakpoint: 500,
                      settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                      }
                    },
                ]
            })
            
        } else {
            galeriaFoto();
        }
    }

    function btnNegociar() {

        $(document).on( 'nfFormReady', function() {
            $('.imovel-btn').on('click', function(e) {
                
                e.preventDefault();

                window.scroll({
                    top: $('#vamosnegociar').offset().top, 
                    left: 0, 
                    behavior: 'smooth' 
                });

                $(window).on('scroll', function() {
                    if($(window).scrollTop() == $('#vamosnegociar').offset().top) {
                        $('.input_nome').focus();
                    }
                });
                return false;
        
            })
        });

    }

    function init() {
        //galeriaFoto();
        slickGaleria();
        btnNegociar();
    }

    init();


} )( jQuery );



// function initMap() {

//     var myLatLng = {lat: -25.363, lng: 131.044};

//     // Create a map object and specify the DOM element
//     // for display.
//     var map = new google.maps.Map(document.getElementById('mapa'), {
//         center: myLatLng,
//         zoom: 12
//     });

//     // Create a marker and set its position.
//     var marker = new google.maps.Marker({
//         map: map,
//         position: myLatLng,
//     });
// }