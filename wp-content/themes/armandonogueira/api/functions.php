<?php 
require_once('config.php');

/*Recuper número total de ímoveiss*/
function totalImoveis($filtro){
    $filtro = json_encode($filtro);
	$responseJson = my_file_get_contents('http://app.smart.youdigital.com.br/sites/v1/imovel/totalImoveisFiltro?filtro='.$filtro);
	$response = json_decode($responseJson);
	return $response->total;
}

/* Lista imóveis a partir de uma filtragem */
function listarImoveis($filtro) {
    $filtro = json_encode($filtro);
	$responseJson = my_file_get_contents('http://app.smart.youdigital.com.br/sites/v1/imovel/listarImoveisFiltro?filtro='.$filtro);
	$response = json_decode($responseJson);
	return $response;
}

function getImovel($imovelId) {
	$responseJson = my_file_get_contents('http://app.smart.youdigital.com.br/sites/v1/imovel/'.$imovelId.'/'.APITOKEN);
	$response = json_decode($responseJson);
	return $response->imovel;
}

function listarImoveisSimilares($imovelId) {
	$responseJson = my_file_get_contents('http://app.smart.youdigital.com.br/sites/v1/imovel/imoveisSugeridos/'.$imovelId.'/5/'.APITOKEN);
	$response = json_decode($responseJson);
	return $response->imoveis;
}

function formatarEndereco($imovel) {
	return $imovel->endereco.', '.$imovel->numero.' - '.$imovel->nomeBairro.' - '.$imovel->nomeCidade.' - '.$imovel->siglaEstado;
}

function formatarArea($imovel) {
	return $imovel->areautil.' '.$imovel->unidade2;
}

function existeArea($imovel) {
	if($imovel->areautil) :
		$return = '<div class="info info-tamanho">
			<img src="'.ASSETS.'/img/icons/tamanho.svg">
			<small>'.formatarArea($imovel).'</small>
		</div>';
	endif;
	return $return;
}

function formatarQuartos($imovel) {
	return $imovel->nquartos > 1 ? $imovel->nquartos.' '.$GLOBALS['campos']['config']['geral']['quartos'] : $imovel->nquartos.' '.$GLOBALS['campos']['config']['geral']['quarto'];
}

function formatarBanheiros($imovel) {
	return $imovel->nbanheirossociais > 1 ? $imovel->nbanheirossociais.' '.$GLOBALS['campos']['config']['geral']['banheiros'] : $imovel->nbanheirossociais.' '.$GLOBALS['campos']['config']['geral']['banheiro'];
}

function formatarVagas($imovel) {
	return $imovel->ngaragens > 1 ? $imovel->ngaragens.' '.$GLOBALS['campos']['config']['geral']['vagas'] : $imovel->ngaragens.' '.$GLOBALS['campos']['config']['geral']['vaga'];
}

function formatarElevadores($imovel) {
	return $imovel->nelevadores > 1 ? $imovel->nelevadores.' '.$GLOBALS['campos']['config']['geral']['elevadores'] : $imovel->nelevadores.' '.$GLOBALS['campos']['config']['geral']['elevadores'];
}

add_action ('wp_ajax_nopriv_get_imoveis', 'get_imoveis');
add_action ('wp_ajax_get_imoveis', 'get_imoveis');

function get_imoveis () {

	$query_vars = json_decode( stripslashes( $_POST['query_vars'] ), true );
	$get = $_POST['get'];
	$post = $_POST['post'];
	$filters = $_POST['filters'];
	$apitoken = APITOKEN;
	$codigoConstrutora = $post['construtora'];
	$qtdImoveis = 6;

	$filtro = array(
		"token" => $apitoken,
		"quantidadeImoveis" => $qtdImoveis,
		"temFoto" => "s"
	);

	if($filters) {
		$filtro += $filters;
	}

	switch ($query_vars['imoveis_cat']) {
		case 'lancamentos':
			$title = 'Lançamentos';
			
			$filtro += ["situacaoEmpreendimento" => 1];
	
			break;
		case 'em-construcao':
			$title = "Em Construção";
	
			$filtro += ["estagioObraStr" => "2;3;4;5"];
	
			break;
		case 'pronto':
			$title = "Pronto para morar";
	
			$filtro += ["estagioObraStr" => "6"];

			break;
		case 'litoral':
			$title = "Litoral";
	
			$filtro += ["litoral" => true];
	
			break;
		case 'empresarial':
			$title = "Empresarial";
	
			$filtro += ["tipoImovel" => 6];
	
			break;
		case 'minha-casa-minha-vida':
			$title = "Minha Casa Minha Vida";
	
			$filtro += ["popular" => true];

			break;
			
		case 'busca':
			$title = "Resultado de busca";

			switch($post['tipo']) {
				case 'apartamentos':

					$filtro += ["tipoImovel" => 1];

					break;
				case 'litoral':

					$filtro += ["litoral" => true];

					break;
				case 'flat':

					$filtro += ["tipoImovel" => 8];

					break;
				case 'comerciais':

					$filtro += ["tipoImovel" => 6];

					break;
			}

			break;
	}

	$response = listarImoveis($filtro);
	$imoveis = $response->imoveis;

	if($codigoConstrutora) {

		$imoveisFiltrados = array();

		foreach($imoveis as $imovel) {
			if($imovel->construtora == $codigoConstrutora) {
				array_push($imoveisFiltrados, $imovel);
			}
		}
		
		// $imoveisFiltrados = array_filter($imoveis, function($i) use ($codigoConstrutora) {
		// 	if($i->preco == 500000) {
		// 		return true;
		// 	}
		// 	return false;
		// 	// if (stripos($i['preco'], $preco) !== false) {
		// 	// 	return true;
		// 	// }
		// 	// return false;
		// });

		$totalImoveis = count($imoveisFiltrados);

		// header('Content-type: application/json');

		// $return = array('imoveis' => $imoveisFiltrados);
		// echo json_encode($return);
	
		// die();
				
	} else {
		$totalImoveis = totalImoveis($filtro);
		$imoveisFiltrados = $imoveis;
	}

	if($imoveisFiltrados) {

	header('Content-type: application/json');

	$html = array();

	//var_dump($post['construtora']);

	foreach($imoveisFiltrados as $imovel) { 
		// ob_start();
		// 	//include(BASE_TEMA.'/partials/imovel-card.php');
		// 	get_template_part( BASE_TEMA.'/partials/imovel', 'card' );
		// $gtp = ob_get_clean();

		$fotos = array();

		$i=0; foreach($imovel->fotoImovelList as $foto) { 
		 array_push($fotos, '<img src="'.$foto->url.'">');
		if($i == 4) : break; endif; $i++; } 


		array_push($html, '<div class="col-sm-6">
			<div class="list-item imovel-card ">
				<a href="'.ROTA_IMOVELDETALHE.$imovel->codigoImovel.'">
				<div class="imovel-container">
					<div class="imovel-content">
						<div class="imovel-img">
							<div class="images" data-imovelcard-slick>
								<img src="'.$imovel->fotoImovelList[0]->url.'">
								<img src="'.$imovel->fotoImovelList[1]->url.'">
								<img src="'.$imovel->fotoImovelList[2]->url.'">
								<img src="'.$imovel->fotoImovelList[3]->url.'">
								<img src="'.$imovel->fotoImovelList[4]->url.'">
							</div>
						</div>

						<div class="imovel-details">
							<header class="imovel-title">
								<h2 class="imovel-bairro">'.$imovel->nomeBairro.'</h2>
								<h3 class="imovel-nome">'.$imovel->nomeImovel.'</h3>
								<i class="arrow-hover gray"></i>	

							</header>
							<div class="imovel-info">
								'.existeArea($imovel).'
								<div class="info info-quartos">
									<img src="'.ASSETS.'/img/icons/quarto.svg">
									<small>'.formatarQuartos($imovel).'</small>
								</div>
								<div class="info info-banheiros">
									<img src="'.ASSETS.'/img/icons/banheiro.svg">
									<small>'.formatarBanheiros($imovel).'</small>
								</div>
							</div>

							<div class="imovel-description">'.$imovel->descricao.'</div>

							<div class="imovel-price">
								<small>A partir de</small>
								<strong class="price">
									<small class="price-cifrao">R$</small>
									<span class="price-valor">'.number_format($imovel->preco, 2, ',', '.').'</span>
								</strong>
							</div>
						</div>
					</div>


				</div>
				</a>
				
			</div>
		</div>');
		
	}

	$return = array('html' => $html, 'imoveis' => $imoveisFiltrados, 'total' => $totalImoveis);
	echo json_encode($return);
	die();

	} else {
		if($codigoConstrutora) {
			header('HTTP/1.1 500 Nenhum imóvel encontrado');
			header('Content-Type: application/json; charset=utf-8');
			die('Nenhum imóvel encontrado');
		} else {
			header('HTTP/1.1 500 '.$response->mensagem);
			header('Content-Type: application/json; charset=utf-8');
			die($response->mensagem);
		}
	}
}


?>