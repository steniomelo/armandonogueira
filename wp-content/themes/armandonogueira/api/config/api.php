<?php

define('APITOKEN', 'ipQoRmpC6zwuXCyU2KYaJqpw5AuFtM6dIppvKVam'); 

function myUrlEncode($string) {
	$entities = array('%22', '%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D');
	$replacements = array('"', '!', '*', "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]");
	return str_replace($entities, $replacements, urlencode($string));
}

function my_file_get_contents( $site_url ){
	$url = myUrlEncode($site_url);
	$ch = curl_init();	
	$timeout = 10;	
	curl_setopt ($ch, CURLOPT_URL, $url);	
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);	           
	curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);	
	$file_contents = curl_exec($ch);	
	curl_close($ch);	
	return $file_contents;
}

?>