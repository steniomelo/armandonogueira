
<div class="list-item imovel-card ">
    <a href="<?php echo ROTA_IMOVELDETALHE.$imovel->codigoImovel;?>">
    <div class="imovel-container">
        <div class="imovel-content">
            <div class="imovel-img">
				<div class="images" data-imovelcard-slick>
                    <?php $i=0; foreach($imovel->fotoImovelList as $foto) {  ?>
                        <img src="<?php echo $foto->url; ?>">
                    <?php if($i == 4) : break; endif; $i++; } ?>
                </div>
            </div>

            <div class="imovel-details">
                <header class="imovel-title">
                    <h2 class="imovel-bairro"><?php echo $imovel->nomeBairro; ?></h2>
                    <h3 class="imovel-nome"><?php echo $imovel->nomeImovel; ?></h3>
                    <i class="arrow-hover gray"></i>	
                </header>
                <div class="imovel-info">
                    <?php if($imovel->areautil) : ?>
                    <div class="info info-tamanho">
                        <img src="<?php echo ASSETS;?>/img/icons/tamanho.svg">
                        <small><?php echo formatarArea($imovel); ?></small>
                    </div>
                    <?php endif; ?>
                    <div class="info info-quartos">
                        <img src="<?php echo ASSETS;?>/img/icons/quarto.svg">
                        <small><?php echo formatarQuartos($imovel); ?></small>
                    </div>
                    <div class="info info-banheiros">
                        <img src="<?php echo ASSETS;?>/img/icons/banheiro.svg">
                        <small><?php echo formatarBanheiros($imovel); ?></small>
                    </div>
                </div>

                <div class="imovel-description">
                    <?php echo $imovel->descricao; ?>
                </div>

                <div class="imovel-price">
                    <small>A partir de</small>
                    <strong class="price">
                        <small class="price-cifrao">R$</small>
                        <span class="price-valor"><?php echo number_format($imovel->preco, 2, ',', '.'); ?></span>
                    </strong>
                </div>
            </div>
        </div>


    </div>
    </a>
    
</div>
