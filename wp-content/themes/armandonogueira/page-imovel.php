<?php
/**
 * The template for displaying all pages.
 *
 * @link https://livecomposerplugin.com/themes/
 *
 * @package LC Blank
 */

get_header(); ?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <?php 

            $imovelId = get_query_var('imovel_id');
            $imovel = getImovel($imovelId);

            
        
        ?>

        <!-- <pre>
            <?php //print_r($imovel); ?>
        </pre> -->

    <div id="imovel-detalhe">
        <header class="imovel-header">
            <div class="container">
                <div class="imovel-title mr-auto">
                    <h2 class="imovel-bairro"><?php echo $imovel->nomeBairro; ?></h2>
                    <h3 class="imovel-nome"><?php echo $imovel->nomeImovel; ?></h3>
                </div>

                <!-- <div class="imovel-construtora d-none d-sm-block">
                    <small>Queiroz Galvão</small>
                </div> -->

                <div class="imovel-price">
                    <hr class="d-none d-sm-block">
                    <small><?php echo $GLOBALS['campos']['config']['geral']['a_partir_de']; ?></small>
                    <strong class="price">
                        <small class="price-cifrao">R$</small>
                        <span class="price-valor"><?php echo number_format($imovel->preco, 2, ',', '.'); ?></span>
                    </strong>
                </div>

                <a href="#vamosnegociar" class="imovel-btn d-none d-sm-flex">
                    <span><?php echo $GLOBALS['campos']['config']['geral']['vamos_negociar']; ?></span>
                    <i class="arrow-hover"></i>
                </a>
            </div>
        </header>

        <div class="imovel-destaque tab-content">
            <?php if($imovel->fotoImovelList) { ?>
                <div class="tab-pane fade show active" id="imovel-galeria" role="tabpanel" >
                    <span class="arrow arrow-prev">
                        <img src="<?php echo ASSETS;?>/img/icons/arrow-slider.svg">
                    </span>
                    <div class="imovel-galeria" data-destaque-galeria>
                        <?php $i=0; foreach($imovel->fotoImovelList as $foto) {  ?>
                            <div class="galeria-foto" data-lazy="<?php echo $foto->url; ?>" style="background-image: url('<?php echo $foto->url; ?>')"></div>
                        <?php if($i == 10) : break; endif; $i++; } ?>
                    </div>
                    <span class="arrow arrow-next">
                        <img src="<?php echo ASSETS;?>/img/icons/arrow-slider.svg">
                    </span>
                </div>
            <?php } ?>
                <div class="tab-pane fade <?php echo !$imovel->fotoImovelList ? 'show active' : ''; ?>" id="imovel-mapa" role="tabpanel" aria-labelledby="profile-tab">
                    
                <?php 
                
                    // $request = file_get_contents("https://maps.google.com/maps/api/geocode/json?key=".GOOGLE_API."&address=".$imovel->endereco."+".$imovel->nomeBairro."+".$imovel->nomeCidade."+".$imovel->siglaEstado."");
                    // $json = json_decode($request, true);

                ?>
                
                    <div id="mapa"></div>
                       
                </div>

                <div class="tab-pane fade" id="imovel-streetview" role="tabpanel" aria-labelledby="contact-tab">
                    <div id="street-view"></div>
                </div>

        </div>

        <div class="imovel-container container">
            <div class="row">
                <div class="col-sm-7">

                    <ul class="nav nav-pills" id="pills-tab" role="tablist">
                        <?php if($imovel->fotoImovelList) { ?>
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#imovel-galeria" role="tab" aria-controls="pills-home" aria-selected="true">
                                <img src="<?php echo ASSETS;?>/img/icons/camera.svg" alt="">
                                <?php echo $GLOBALS['campos']['config']['geral']['fotografias']; ?>
                            </a>
                        </li>
                        <?php } ?>
                        <li class="nav-item">
                            <a class="nav-link <?php echo !$imovel->fotoImovelList ? 'active' : ''; ?>" id="pills-profile-tab" data-toggle="pill" href="#imovel-mapa" role="tab" aria-controls="pills-profile" aria-selected="false">
                                <img src="<?php echo ASSETS;?>/img/icons/map.svg" alt="">
                                <?php echo $GLOBALS['campos']['config']['geral']['localizacao']; ?>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link vizinhanca" id="pills-contact-tab" data-toggle="pill" href="#imovel-streetview" role="tab" aria-controls="pills-contact" aria-selected="false">
                                <img src="<?php echo ASSETS;?>/img/icons/tree.svg" alt="">
                                <?php echo $GLOBALS['campos']['config']['geral']['vizinhanca']; ?>
                            </a>
                        </li>
                    </ul>

                    <div class="imovel-content">
                        
                        <header class="content-header">
                            <div class="content-title">
                                <h1><?php echo $imovel->nomeImovel; ?></h1>
                                <p><?php echo formatarEndereco($imovel); ?></p>
                            </div>
                        </header>

                        <div class="imovel-info">
                            <?php if($imovel->areautil) : ?>
                                <div class="info">
                                    <img src="<?php echo ASSETS;?>/img/icons/tamanho.svg">
                                    <small><?php echo formatarArea($imovel); ?></small>
                                </div>
                            <?php endif; ?>
                            <div class="info">
                                <img src="<?php echo ASSETS;?>/img/icons/quarto.svg">
                                <small><?php echo formatarQuartos($imovel); ?></small>
                            </div>
                            <div class="info">
                                <img src="<?php echo ASSETS;?>/img/icons/banheiro.svg">
                                <small><?php echo formatarBanheiros($imovel); ?></small>
                            </div>
                            <div class="info">
                                <img src="<?php echo ASSETS;?>/img/icons/vaga.svg">
                                <small><?php echo formatarVagas($imovel); ?></small>
                            </div>
                            <?php if($imovel->temEspacoGourmet) { ?>
                                <div class="info">
                                    <img src="<?php echo ASSETS;?>/img/icons/gourmet.svg">
                                    <small>Gourmet</small>
                                </div>
                            <?php } ?>
                            <?php if($imovel->temArcondicionadoSplit) { ?>
                                <div class="info">
                                    <img src="<?php echo ASSETS;?>/img/icons/split.svg">
                                    <small>Air Split</small>
                                </div>
                            <?php } ?>
                        </div>

                        <div class="imovel-description">
                            <h2><?php echo $GLOBALS['campos']['config']['geral']['empreendimento']; ?></h2>

                            <p class="description"><?php echo $imovel->descricao; ?></p>

                            <div class="imovel-more-info">
                                <?php if($imovel->nelevadores) : ?>
                                    <div class="info">
                                        <img src="<?php echo ASSETS;?>/img/icons/elevator.svg">
                                        <small><?php echo formatarElevadores($imovel); ?></small>
                                    </div>
                                <?php endif; ?>

                                <?php if($imovel->nandares) : ?>
                                    <div class="info">
                                        <img src="<?php echo ASSETS;?>/img/icons/andares.svg">
                                        <small><?php echo $imovel->nandares ?> <?php echo $GLOBALS['campos']['config']['geral']['andares']; ?></small>
                                    </div>
                                <?php endif; ?>

                                <?php if($imovel->temSalaGinastica) : ?>
                                    <div class="info">
                                        <img src="<?php echo ASSETS;?>/img/icons/ginastica.svg">
                                        <small><?php echo $GLOBALS['campos']['config']['geral']['sala_de_ginastica']; ?></small>
                                    </div>
                                <?php endif; ?>

                                <?php if($imovel->temPiscina) : ?>
                                    <div class="info">
                                        <img src="<?php echo ASSETS;?>/img/icons/piscina.svg">
                                        <small><?php echo $GLOBALS['campos']['config']['geral']['piscina']; ?></small>
                                    </div>
                                <?php endif; ?>

                                <?php if($imovel->temPlayground) : ?>
                                    <div class="info">
                                        <img src="<?php echo ASSETS;?>/img/icons/playground.svg">
                                        <small>Playground</small>
                                    </div>
                                <?php endif; ?>

                                <?php if($imovel->temSalaoFestas) : ?>
                                    <div class="info">
                                        <img src="<?php echo ASSETS;?>/img/icons/salao.svg">
                                        <small><?php echo $GLOBALS['campos']['config']['geral']['salao_de_festas']; ?></small>
                                    </div>
                                <?php endif; ?>

                                <?php if($imovel->salaoJogos) : ?>
                                    <div class="info">
                                        <img src="<?php echo ASSETS;?>/img/icons/jogos.svg">
                                        <small><?php echo $GLOBALS['campos']['config']['geral']['salal_de_jogos']; ?></small>
                                    </div>
                                <?php endif; ?>

                            </div>

                        </div>
                    </div>
                </div>

                <div id="vamosnegociar" class="col-sm-5 sidebar">
                    <?php 
                    if ( qtrans_getLanguage() == 'pb' ) {
                        echo do_shortcode('[ninja_form id=1]');
                    } else {
                        echo do_shortcode('[ninja_form id=5]');
                    }
                     ?>
                </div>
            </div>

            
           
        </div>
    </div>
    
    <?php 
    $imoveisSimilares = listarImoveisSimilares($imovelId);

    if($imoveisSimilares): ?>
        <section id="imoveis-similares" class="section list-similares">

            <div class="section-content">
                <div class="list-horizontal" >
                    <div class="list-items list-imoveis" data-list-horizontal>
                        <header class="section-header">
                            <h2 class="section-title"><?php echo $GLOBALS['campos']['config']['geral']['similares']; ?></h2>
                        </header>

                        <?php 
                        foreach($imoveisSimilares as $imovel) {  ?>
                            <?php 
                                set_query_var( 'imovel', $imovel );
                                get_template_part( 'partials/imovel', 'card' );
                            ?>
                        <?php } ?>
                        <div class="space"></div>
                    </div>
                </div>
            </div>
        </section>
	<?php endif; ?>


	<?php endwhile; ?>
	<?php endif; ?>

<script>

var latLng;

function init() {


    $.ajax({
        url: 'https://maps.google.com/maps/api/geocode/json?key=<?php echo GOOGLE_API; ?>&address=<?php echo $imovel->endereco; ?>+<?php echo $imovel->nomeBairro?>+<?php echo $imovel->nomeCidade;?>+<?php echo $imovel->siglaEstado?>',
        type: 'GET',
        success: function(response) {

            if(response.status == 'OK')
            latLng = response.results[0].geometry.location;


            console.log(latLng);
            initMap();
            //streetView();
        }
    })
}

var panorama;
function streetView() {
    panorama = new google.maps.StreetViewPanorama(
    document.getElementById('street-view'),
    {
        position: latLng,
        pov: {heading: 165, pitch: 0},
        zoom: 1
    });
}

function initMap() {
    console.log(latLng);
    var myLatLng = latLng;

    // Create a map object and specify the DOM element
    // for display.
    var map = new google.maps.Map(document.getElementById('mapa'), {
    center: myLatLng,
    zoom: 16
    });

    // Create a marker and set its position.
    var marker = new google.maps.Marker({
        map: map,
        position: myLatLng,
    });

    // var panorama = new google.maps.StreetViewPanorama(
    // document.getElementById('street-view'),
    // {
    //     position: latLng,
    //     pov: {heading: 165, pitch: 10},
    //     zoom: 2
    // });
    //map.setStreetView(panorama);
}

</script>


<?php
wp_enqueue_script('google-maps');
wp_enqueue_script('imovel-detalhe');
get_footer(); 
?>
