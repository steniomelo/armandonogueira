<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://www.steniomelo.com.br
 * @since      1.0.0
 *
 * @package    Ang_Imoveis
 * @subpackage Ang_Imoveis/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
