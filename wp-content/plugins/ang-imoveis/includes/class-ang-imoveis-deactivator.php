<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.steniomelo.com.br
 * @since      1.0.0
 *
 * @package    Ang_Imoveis
 * @subpackage Ang_Imoveis/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Ang_Imoveis
 * @subpackage Ang_Imoveis/includes
 * @author     Stênio Figueiredo de Melo <stenio@steniomelo.com.br>
 */
class Ang_Imoveis_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
