<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://www.steniomelo.com.br
 * @since      1.0.0
 *
 * @package    Ang_Imoveis
 * @subpackage Ang_Imoveis/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Ang_Imoveis
 * @subpackage Ang_Imoveis/includes
 * @author     Stênio Figueiredo de Melo <stenio@steniomelo.com.br>
 */
class Ang_Imoveis_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'ang-imoveis',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
